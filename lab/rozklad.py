# -*- coding: utf-8 -*-
from math import *
import memcache
from random import randrange
import timeit
import random

mcache = memcache.Client(['194.29.175.241:2207', '194.29.175.242:2207'])



def rozklad_liczb(x):
    end = x
    cache  = mcache.get('p11' + str(x))
    if cache is not None:
        return cache
    if x<=0:
        return 0
    i=2
    e=floor(sqrt(x))
    r=[] #używana jest tablica (lista), nie bepośrednie wypisywanie
    while i<=e:
        if x%i==0:
            r.append(i)
            x/=i
            cache = mcache.get('p11' + str(x))
            if cache is not None:
                r = r + cache
                return r
            e=floor(sqrt(x))
        else:
            i+=1
    if x>1: r.append(x)
    mcache.set('p11' + str(end),r)
    return r

def p():
    print timeit.timeit()

czas = timeit.timeit()
print(czas)
for x in range(0,445):
        l = randrange(444, 4444)
        r=rozklad_liczb(l)
        print(str(x) + " | "+str(l)+" : " + str(r))
czas1 = timeit.timeit()
print(czas1)
print(czas1-czas)

