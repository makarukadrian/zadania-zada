Praca domowa #A
===============

Korzystając z dowolnego frameworka webowego stworzyć projekt korzystający
z rozproszonych zadań (conajmniej dwojakiego rodzaju) realizowanych w tle z kilkoma workerami.
Interfejs ma być przyjazny dla użytkownika i w żadnym wypadku się nie zacinać.
Dla długotrwałych zadań (np. pakowanie lub rozpakowywanie pliku zip) należy wyświetlić użytkownikowi odpowiednią informację oraz pasek postępu.


Przykładowy projekt
-------------------

Galeria zdjęć dla wielu uzytkowników posiadająca następujące funkcjonalności:

* Dodawanie pojedynczego zdjęcia
* Dodawanie pliku zip z wieloma zdjęciami, które aplikacja rozpakowuje i wrzuca do galerii
* Tworzenia katalogów i przenoszenie zdjęć między nimi
* Wyświetlanie zawartości katalogu w postaci miniatur o różnych wielkościach (co najmniej 3 do dyspozycji).
  Miniatury powinny być przeskalowanymi obrazkami po stronie serwera a nie pełnowymiarowymi tylko przeskalowanymi
  po stronie przeglądarki za pomocą styli, atrybutów html czy javascriptu.
* Zapisywanie pojedynczego zdjęcia na dysk.
* Zapisywanie wszystkich zdjęć z wybranego katalogu w postaci pliku zip.



from math import *
import memcache, random, timeit
#mc = memcache.Client(['194.29.175.241:11211','194.29.175.242:11211'])
mc = memcache.Client(['194.29.175.241:11211'])


found = 0
total = 0

def liczby_pierwsze(x):
    _input = x
    if x <= 0:
        return 0
    i = 2
    e = floor(sqrt(x))
    r = []
    while i <= e:
        if x%i==0:
            r.append(i)
            x /= i
            e = floor(sqrt(x))
        else:
            i+=1
    if x>1:
        r.append(x)
    if _input in r:
        return _input
    return r



def main(x):
    global total, found
    total += 1
    value = mc.get('p7:%d' % x)
    if value is None:
        value = liczby_pierwsze(x)
        try:
            value = str(value)
        except:
            pass
        mc.set('p7:%d' % x, value)
    else:
        found += 1
    return value

def make_request():
    liczba = random.randint(2, 10000)
    return main(liczba)
    timeit.timeit()
    #return main(350)
print 'Ten successive runs:',
for i in range(1, 10):
    liczba = random.randint(2, 10000)
    print '%.2f' % (timeit.timeit(make_request, number=100)) + ' ' + make_request()
    print
